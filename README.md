# Appium

Appium is an open source automation tool for running scripts and testing native applications, mobile-web applications and hybrid applications on Android or iOS using a webdriver.

## Official Documentation

Documentation for the appium can be found on the [Appium website](http://appium.io/docs/en/about-appium/api/).

## What's Added

- [selenium-java]
- [Appium]
- [TestNG]
- [io.appium]

## Quick Start

- Clone this repo or download it's release archive and extract it somewhere
- You may delete `.git` folder if you get this code via `git clone`
- Configure your `PLATFORM_NAME`,`PLATFORM_VERSION`,`DEVICE_NAME`,`UDID`in file BaseClass.java
- Make sure fast internet connection and network, run and happy testing

By Diansa Putra 2021


