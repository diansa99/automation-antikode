package pages;

import java.awt.List;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import tests.BaseClass;

public class PlayStore {
	
		private AppiumDriver driver;
		private WebDriverWait wait;
		private AndroidDriver androidDriver;

	    // app to install - details
	    final String testAppName = "Chromecast";
	    final String testAppPackage = "com.google.android.apps.chromecast.app";
	    final String testAppActivity = ".DiscoveryActivity";
	    
	    @BeforeTest
	    public void setUp() throws Exception {
	    	DesiredCapabilities capabilities = new DesiredCapabilities();
	        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy A70");
	        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
	        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
	        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
	        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.vending");
	        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".AssetBrowserActivity");
	        capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, ".AssetBrowserActivity");
	        capabilities.setCapability("newCommandTimeout", 0);
	        
	        URL url = new URL("http://127.0.0.1:4723/wd/hub");
			driver = new AppiumDriver<MobileElement>(url, capabilities);
	        
	    }
	    
	    @Test
	    public void installAppFromGooglePlayStore() throws Exception {
	    	BaseClass.wait(3);
	    	driver.findElement(MobileBy.className("android.widget.FrameLayout")).click();
	    	driver.findElement(MobileBy.className("android.widget.EditText")).sendKeys("My home credit");
	    	String cmd = "adb shell input keyevent 66";
	    	Runtime.getRuntime().exec(cmd);
	    	BaseClass.wait(3);
	    	driver.findElement(MobileBy.className("android.widget.Button")).click();
	    	wait(200);
	    	driver.findElement(MobileBy.className("android.widget.Button")).click();
	    	
	    	
	    	System.out.println("install succesfully");
	    }
	    
//	    @AfterTest
//	    public void tearDown() throws Exception {
////	        driver.quit();
//	    }
}
