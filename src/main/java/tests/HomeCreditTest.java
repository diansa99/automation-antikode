package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;

public class HomeCreditTest extends BaseClass {
	
	@Test(priority = 2)
	public void HomeCreditTest() {
		
		this.swipeScreen(Direction.LEFT, 3);
		this.clickRegisterButton();
		this.swipeScreen(Direction.UP, 3);
		this.clickAgreeButton();
		this.registerAccount();
		this.agreePopUp();
		
		System.out.println("RUN");
		
	}

}
