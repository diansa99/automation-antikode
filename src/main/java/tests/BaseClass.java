package tests;

import java.net.URL;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class BaseClass {
	
	AppiumDriver<MobileElement> driver;

	@BeforeTest
	public void setup() {
		
		try {
		
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy A70");
			cap.setCapability(MobileCapabilityType.UDID, "RR8M5117SZP");
//			cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "id.co.myhomecredit");
//			cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "id.co.homecredit.v2.launcherpage.SplashScreenActivity");
			
			cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.vending");
			cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.android.vending.AssetBrowserActivity");
			
			cap.setCapability("newCommandTimeout", 0);
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
			driver = new AppiumDriver<MobileElement>(url, cap);
		
		} catch (Exception e) {
			System.out.println("Cause is :"+e.getCause());
			System.out.println("Message is :"+e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	@Test(priority = 1)
	public void sampleTest() throws Exception {
		System.out.println("Application run!");
		wait(3);
		driver.findElement(MobileBy.className("android.widget.FrameLayout")).click();
		driver.findElement(MobileBy.className("android.widget.EditText")).sendKeys("My home credit");
    	String cmd = "adb shell input keyevent 66";
    	Runtime.getRuntime().exec(cmd);
    	wait(3);
    	driver.findElement(MobileBy.className("android.widget.Button")).click();
    	BaseClass.wait(200);
    	driver.findElement(MobileBy.className("android.widget.Button")).click();
    	System.out.println("install succesfully");
	}
	
	@AfterTest
	public void teardown() {
		driver.close();
		driver.quit();
	}
	
	public static void wait(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clickRegisterButton() {
		MobileElement btnRegister = driver.findElement(By.id("id.co.myhomecredit:id/button_register"));
		btnRegister.click();
	}
	
	public void clickAgreeButton() {
		wait(3);
		MobileElement btnAgree = driver.findElement(By.id("id.co.myhomecredit:id/agree_button"));
		btnAgree.click();
	}
	
	public void registerAccount() {
		wait(3);
		MobileElement dateOfBirth = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[1]/android.widget.Spinner[1]"));
		dateOfBirth.click();
		swipeScreen(Direction.UP, 1);
		MobileElement selectDateNumber = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[6]"));
		selectDateNumber.click();
		MobileElement monthOfBirth = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[1]/android.widget.Spinner[2]"));
		monthOfBirth.click();
		MobileElement selectMonthOfBirth = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]"));
		selectDateNumber.click();
		MobileElement yearOfBirth = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[1]/android.widget.Spinner[3]"));
		yearOfBirth.click();
		swipeScreen(Direction.DOWN, 1);
		MobileElement selectYearOfBirth = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[11]"));
		selectYearOfBirth.click();
		MobileElement phoneInput = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[2]/android.view.View/android.widget.EditText"));
		phoneInput.click();
		phoneInput.sendKeys("81211664801");
		MobileElement kodePin = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[3]/android.view.View/android.widget.EditText"));
		kodePin.click();
		kodePin.sendKeys("123123");
		MobileElement kodePinConfirm = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[4]/android.view.View/android.widget.EditText"));
		kodePinConfirm.click();
		kodePinConfirm.sendKeys("123123");
		
		MobileElement registerButton = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[3]/android.view.View[5]/android.widget.Button"));
		registerButton.click();
	}
	
	public void agreePopUp() {
		wait(3);
		MobileElement agreePopUp = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View[4]/android.view.View/android.view.View[6]/android.widget.Button"));
		agreePopUp.click();
	}
	
	public enum Direction {
	    UP,
	    DOWN,
	    LEFT,
	    RIGHT;
	}
	
	public void swipeScreen(Direction dir, int totalLoop) {
		wait(5);
		System.out.println("swipeScreen(): dir: '" + dir + "'");
		final int ANIMATION_TIME = 200;
	    final int PRESS_TIME = 200;
	    
	    int edgeBorder = 10; // better avoid edges
	    PointOption pointOptionStart, pointOptionEnd;
	    
	    Dimension dims = driver.manage().window().getSize();
	    pointOptionStart = PointOption.point(dims.width / 2, dims.height / 2);
	    
	    for (int i = 0; i < totalLoop; i++) {
	    
		    switch (dir) {
		        case DOWN: 
		            pointOptionEnd = PointOption.point(dims.width / 2, dims.height - edgeBorder);
		            break;
		        case UP:
		            pointOptionEnd = PointOption.point(dims.width / 2, edgeBorder);
		            break;
		        case LEFT: 
		            pointOptionEnd = PointOption.point(edgeBorder, dims.height / 2);
		            break;
		        case RIGHT: 
		            pointOptionEnd = PointOption.point(dims.width - edgeBorder, dims.height / 2);
		            break;
		        default:
		            throw new IllegalArgumentException("swipeScreen(): dir: '" + dir + "' NOT supported");
		    }
		    
		    try {
		        new TouchAction(driver)
		                .press(pointOptionStart)
		                // a bit more reliable when we add small wait
		                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
		                .moveTo(pointOptionEnd)
		                .release().perform();
		    } catch (Exception e) {
		        System.err.println("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
		        return;
		    }
	    
	    }

	    try {
	        Thread.sleep(ANIMATION_TIME);
	    } catch (InterruptedException e) {
	    }
	}

}
